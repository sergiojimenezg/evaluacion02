<%-- 
    Document   : salida
    Created on : 26-04-2020, 10:15:55
    Author     : sjimenez
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="root.model.entities.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Cliente> cliente = new ArrayList<Cliente>();
    
    if(request.getAttribute("cliente")!=null){
        cliente=(List<Cliente>)request.getAttribute("cliente");
    }
    
    Iterator<Cliente> itCliente = cliente.iterator();
%>    
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Lista de Clientes</title>
    </head>
    
        <body class="text-center" >
        <h1>Listado de Clientes</h1>    
        <form name="form" action="controllerListado" method="POST">      
            <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
                <table border="1">
                    <thead>
             
                        <th>Seleccion</th>    
                        <th>Identificacion</th>
                        <th>Nombre </th>
                        <th>Apellido Paterno </th>
                        <th>Apellido Materno </th>
                        <th>Mail </th>
                        <th>Direccion </th>
                        <th>Telefono </th>
    
                 </thead>
                <tbody>
                    <%while(itCliente.hasNext()){
                        Cliente cli=itCliente.next();%>
                        <tr>
                            <td> <input type="radio" name="seleccion" value="<%= cli.getCliIdentificacion()%>"> </td>
                   
                            <td><%= cli.getCliIdentificacion()%></td>
                            <td><%= cli.getCliNombres()%></td>
                            <td><%= cli.getCliApellido1()%></td>
                            <td><%= cli.getCliApellido2()%></td>
                            <td><%= cli.getCliMail()%></td>
                            <td><%= cli.getCliDireccion()%></td>
                            <td><%= cli.getCliTelefono()%></td>
                        
                        </tr>
                    <%}%>              
                </tbody>           
                </table>
                
                <div class="btn-group col-lg-12">
                    <button type="submit" name="accion" value="editar" class="btn btn-default btn-responsive btn-lg btn-success" style="margin: 100px;">Editar</button>
                    <button type="submit" name="accion" value="eliminar" class="btn btn-default btn-responsive btn-lg btn-success" style="margin: 100px;">Eliminar</button>
                    <button type="submit"  name="accion"  value="crear" class="btn btn-default btn-responsive btn-lg btn-success" style="margin: 100px;">Crear</button>
                    <button type="submit"  name="accion"  value="listar" class="btn btn-default btn-responsive btn-lg btn-success" style="margin: 100px;">Listar</button>
                </div>    
     </form>
    
</html>
