<%-- 
    Document   : agregar
    Created on : 26-04-2020, 11:24:05
    Author     : sjimenez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Agregar Cliente</title>
    </head>
    <body>
        <div class="container">
            <div class="col-lg-6"
                <h1>Agregar Usuario</h1>
                <form action="controllerCliente" method="POST">
                    Identificacion:<br>
                    <input class="form-control" type="text" name="identificacion"><br>
                    Nombres:<br>
                    <input class="form-control" type="text" name="nombres"><br>
                    Apellido Paterno:<br>
                    <input class="form-control" type="text" name="apellido1"><br>
                    Apellido Materno<br>
                    <input class="form-control" type="text" name="apellido2"><br>
                    Mail:<br>
                    <input class="form-control" type="text" name="mail"><br>
                    Direccion:<br>
                    <input class="form-control" type="text" name="direccion"><br>
                    Telefono:<br>
                    <input class="form-control" type="text" name="telefono"><br>
                
                    <button type="submit" type="submit" name="accion" value="agregar" class="btn btn-success">Agregar</button>
                </form>
            </div>
        </div>
    </body>
</html>
