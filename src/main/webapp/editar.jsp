<%-- 
    Document   : editar
    Created on : 26-04-2020, 12:24:44
    Author     : sjimenez
--%>

<%@page import="root.model.entities.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
  Cliente cliente=(Cliente)request.getAttribute("cliente");
 
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Editar Cliente</title>
    </head>
    <body>
        <div class="container">
            <div class="col-lg-6">
                <h1>Editar Usuario</h1>
                    <form action="controllerCliente" method="POST">
                        Identificacion:<br>
                        <input class="form-control" type="text" name="identificacion" value="<%=cliente.getCliIdentificacion()%>"><br>
                        Nombres:<br>
                        <input class="form-control" type="text" name="nombres" value="<%=cliente.getCliNombres()%>"><br>
                        Apellido Paterno:<br>
                        <input class="form-control "type="text" name="apellido1" value="<%=cliente.getCliApellido1()%>"><br>
                        Apellido Materno<br>
                        <input class="form-control" type="text" name="apellido2" value="<%=cliente.getCliApellido2()%>"><br>
                        Mail:<br>
                        <input class="form-control" type="text" name="mail" value="<%=cliente.getCliMail()%>"><br>
                        Direccion:<br>
                        <input class="form-control" type="text" name="direccion" value="<%=cliente.getCliDireccion()%>"><br>
                        Telefono:<br>
                        <input type="text" name="telefono" value="<%=cliente.getCliTelefono()%>"><br>
                
                        <button type="submit" type="submit" name="accion" value="editar" class="btn btn-success">Editar</button>
                    </form>
            </div>
            
        </div>
    </body>
</html>
