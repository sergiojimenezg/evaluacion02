/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.ClienteDAO;
import root.model.entities.Cliente;

/**
 *
 * @author sjimenez
 */
@WebServlet(name = "ControllerCliente", urlPatterns = {"/controllerCliente"})
public class ControllerCliente extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        System.out.println("Hola");
        String identificacion = request.getParameter("identificacion");
        String nombres = request.getParameter("nombres");
        String apellido1 = request.getParameter("apellido1");
        String apellido2 = request.getParameter("apellido2");
        String mail = request.getParameter("mail");
        String direccion = request.getParameter("direccion");
        String telefono = request.getParameter("telefono");
        
        String accion = request.getParameter("accion");
        
        
        if (accion.equalsIgnoreCase("agregar")){
            Cliente clientes = new Cliente();
            clientes.setCliIdentificacion(identificacion);
            clientes.setCliNombres(nombres);
            clientes.setCliApellido1(apellido1);
            clientes.setCliApellido2(apellido2);
            clientes.setCliMail(mail);
            clientes.setCliDireccion(direccion);
            clientes.setCliTelefono(telefono);
        
            ClienteDAO dao = new ClienteDAO();
        
        
            try {
                dao.create(clientes);
            
                List<Cliente> cliente=dao.findClienteEntities();
                System.out.println("Cantidad clientes en Base de datos  "+cliente.size())  ;
            
                request.setAttribute("cliente", cliente);
                request.getRequestDispatcher("salida.jsp").forward(request,response);
                Logger.getLogger("log").log(Level.INFO,"valor de identificacion de cliente (0)", clientes.getCliIdentificacion());
            } catch (Exception ex) {
                Logger.getLogger("log").log(Level.SEVERE, "Se presento un error al ingresar cliente", ex.getMessage());
            }
        }
        
         if (accion.equalsIgnoreCase("editar")) {
            Cliente clientes = new Cliente();
            
            clientes.setCliIdentificacion(identificacion);
            clientes.setCliNombres(nombres);
            clientes.setCliApellido1(apellido1);
            clientes.setCliApellido2(apellido2);
            clientes.setCliMail(mail);
            clientes.setCliDireccion(direccion);
            clientes.setCliTelefono(telefono);
        
            ClienteDAO dao = new ClienteDAO();
        
        
            try {
                dao.edit(clientes);
            
                List<Cliente> cliente=dao.findClienteEntities();
                System.out.println("Cantidad clientes en Base de datos  "+cliente.size())  ;
            
                request.setAttribute("cliente", cliente);
                request.getRequestDispatcher("salida.jsp").forward(request,response);
                Logger.getLogger("log").log(Level.INFO,"valor de identificacion de cliente (0)", clientes.getCliIdentificacion());
            } catch (Exception ex) {
                Logger.getLogger("log").log(Level.SEVERE, "Se presento un error al ingresar cliente", ex.getMessage());
            }
        } 
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
